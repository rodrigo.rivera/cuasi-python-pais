#!/usr/bin/env python
# -*- coding: utf-8 -*-
''' multilinea
 comentario line 
implentar un clase  pais que represente un pais con un nombre codigo de pais poblacion por ano impletna una la clase
 contienete que represente un continente con un nombre , codigo de continente  y una lista de paises
para cada una de estas clases implemtea los metos correspondientes para que el sigueinte archvi prubas pase
'''
import json

class Pais:
    nombre = ""
    codigo = ""
    poblacionAño = []
    def __init__(self, nombre, codigo,poblacionAño):
        self.nombre = nombre
        self.codigo = codigo
        self.poblacionAño = poblacionAño
        
    def displayPais(self):
      print ("Name : ", self.nombre , ", Salary: ", self.codigo, " poblacion ",self.poblacionAño)
    
    def poblacion(self,año=None):    
        for t in self.poblacionAño:
            if(año == t[0]):
                return t[1]
        return self.poblacionAño[0][1]

    def asignaploblacion(self, año, tamaño):
        tupla =(año,tamaño)
        self.poblacionAño.insert(0,tupla)
    
    def es_masgrande(self, pais, año=None):
        pais1=-1 
        pais2=-1
        for t in self.poblacionAño:
            if(año == t[0]):
                pais1 = self.poblacionAño[0][1]
        for t in pais.poblacionAño:
            if(año == t[0]):
                pais2 = pais.poblacionAño[0][1]
        if( pais1 < pais2 ):
            return True
        elif(pais1 > pais2):
            return False
        else:
            if(self.poblacionAño[0][1] > pais.poblacionAño[0][1]):
                return True
            else:
                return False
    def __str__(self):
        return str(self.nombre+" tiene una población de "+repr(self.poblacionAño[0][1])+" habitantes")

class Continente:
    nombre =""
    codigo =""
    listaPaises=[]

    def __init__(self, nombre, codigo,listaPaises):
        self.nombre = nombre
        self.codigo = codigo
        self.listaPaises = listaPaises
    
    def poblacion(self,año=None):
        totalAños=0
        for t in self.listaPaises:
            totalAños += t.poblacion(año)
        return totalAños

    def __str__(self):
        self.listaPaises.sort(key=lambda tem: tem.nombre)        
        StringPaises =self.nombre+" ("
        for t in self.listaPaises:
            StringPaises += str(t.nombre+", ")
        temp = len(StringPaises)
        StringPaises =StringPaises[:temp - 2] #quito los dos ultimos caracteres
        StringPaises+=")"
        return str(StringPaises)




def test_pais():
    canada = Pais('Canada', 'CAN', [(2016, 31995000),(2015, 31676000)])
    assert canada.nombre == 'Canada'
    assert canada.codigo == 'CAN'

def tes_pais_poblacion():
    canada = Pais('Canada', 'CAN', [(2016, 31995000),(2015, 31676000)])
    assert canada.poblacion(2016) == 31995000
    assert canada.poblacion(2015) == 31676000
    assert canada.poblacion() == 31995000

def test_pais_asignaploblacion():
    canada = Pais('Canada', 'CAN', [(2016, 31995000),(2015, 31676000)])
    canada.asignaploblacion(2017, 32312000)
    assert canada.poblacion(2017) == 32312000
    assert canada.poblacion(2016) == 31995000
    assert canada.poblacion() == 32312000

def test_pais_es_maspoblado():
    canada = Pais('Canada', 'CAN', [(2016, 31995000), (2015, 31676000)])
    usa = Pais('United States', 'USA', [(2016, 31995001), (2015, 31675000)])
    assert canada.es_masgrande(usa, 2015) is True
    assert canada.es_masgrande(usa) is False

def test_pais_str():
    canada = Pais('Canada', 'CAN', [(2016, 31995000), (2015, 31676000)])
    assert str(canada) == 'Canada tiene una población de 31995000 habitantes'
'''

================TEST CONTIENENTE ============


'''
def test_continente():
    canada = Pais('Canada', 'CAN', [(2016, 31995000), (2015, 31676000)])
    usa = Pais('United States', 'USA', [(2016, 31995001), (2015, 31675000)])
    mexico = Pais('Mexico', 'MEX', [(2016, 127540423), (2015, 112336538)])
    paises = [canada, usa, mexico]
    america_norte = Continente('América del Norte', 'AN', paises)
    assert america_norte.nombre == 'América del Norte'
    assert america_norte.codigo == 'AN'

def test_continente_poblacion():
    canada = Pais('Canada', 'CAN', [(2016, 31995000), (2015, 31676000)])
    usa = Pais('United States', 'USA', [(2016, 31995001), (2015, 31675000)])
    mexico = Pais('Mexico', 'MEX', [(2016, 127540423), (2015, 112336538)])
    paises = [canada, usa, mexico]
    america_norte = Continente('América del Norte', 'AN', paises)
    assert america_norte.poblacion() == 191530424
    assert america_norte.poblacion(2015) == 175687538

def test_continente_str():
    canada = Pais('Canada', 'CAN', [(2016, 31995000), (2015, 31676000)])
    usa = Pais('United States', 'USA', [(2016, 31995001), (2015, 31675000)])
    mexico = Pais('Mexico', 'MEX', [(2016, 127540423), (2015, 112336538)])
    paises = [canada, usa, mexico]
    america_norte = Continente('América del Norte', 'AN', paises)
    assert str(america_norte) == 'América del Norte (Canada, Mexico, United States)'

test_pais()
tes_pais_poblacion()
test_pais_asignaploblacion()
test_pais_es_maspoblado() 
test_pais_str()
test_continente()
test_continente_str()
test_continente_poblacion()



'''
ejerccios de de la segunda parte
'''
def run():
  paisesTxt = getPaisTXT()
  listaPais =crearPais(paisesTxt)   
  #print(listaPais[0].nombre)
  continenteNorthAmerica = getContinente("North America",listaPais)
  continenteSouthAmerica = getContinente("South America",listaPais)
  continenteEurope = getContinente("Europe",listaPais)
  
  
  print ("Poblacion de North America:",continenteNorthAmerica.poblacion(2016))
  print ("Poblacion de South America:",continenteSouthAmerica.poblacion(2016))
  print ("Poblacion de Europe:",continenteNorthAmerica.poblacion(2016))

'''
obtiene el los paises que estan en el contiene que le pasamos por parametro
y el acronimo de el continente
'''
def getContinente(continente,pais):
    listaPais=[]
    
    for p in pais:
        a = temp(continente,p.nombre)
        if(a):
            listaPais.append(p)
    acromino = getAcronimoContiene(continente)        
    return Continente(continente,acromino,listaPais)  

'''
obtener el acronimo de un contiente 
'''
def getAcronimoContiene(continente):     
    lisContinente = json.loads(open('country-and-continent-codes-list_json.json').read())
    for t in lisContinente:
        if t["Continent_Name"] == continente:
           return  t["Continent_Code"]

'''
lee el archvio y revisa si el pais esta en ese contiene 
si esta regresa el codigo de el continente 
'''
def temp(continente,pais):
    lisContinente = json.loads(open('country-and-continent-codes-list_json.json').read())
    for t in lisContinente:
        if t["Continent_Name"] == continente:
            #print("infor bruto :  ",t["Country_Name"])
            if(t["Country_Name"] == pais):
                return True
    return False
            
'''
crea pasise a partir de una lista de nombres
'''
def crearPais(listanombres):
    listaPaises =[]
    for t in listanombres:
        codigo = getCodigo(t)
        lista = poblacionPais(t)
        p = Pais(t,codigo,lista)
        listaPaises.append(p)
    
    return listaPaises

  
'''
obetner el la poblacion de un pais a partir de el nombre 
'''
def poblacionPais(pais):
    #print("el pais ",pais," typo ",len(pais))
    arreglo = []
    poblacion = json.loads(open('population_json.json').read())
    for t in poblacion:        
        #print(t["Country Name"]," = ",pais)        
        if( t["Country Name"] == pais ):
            s=(t["Year"],t["Value"])    
            arreglo.append(s)
    if(len(arreglo)==0): # caso en que no tenga poblacion se asigo uno 
        arreglo.append((0,0))
    
    return arreglo

'''
obetener el codigo de el nombre pais 
'''
def getCodigo(pais):
    paises = json.loads(open('population_json.json').read())
    for t in paises:
        if t["Country Name"] == pais:
            #print(t["Country Code"])
            return t["Country Code"]
    return "no esta"
'''
obtine los nombre de los paises de el arvchivo population_json
'''
def getNombrePaises():
    nombrePais=[]
    #paises = json.loads(open('country-and-continent-codes-list_json.json').read())
    paises = json.loads(open('population_json.json').read())
    for t in paises:
        if t["Country Name"] in nombrePais:
            i=0
        else:
            nombrePais.append(t["Country Name"])     
    #print(len(nombrePais))
    return nombrePais

'''
leer un archivo  y crear una lista
'''
def getPaisTXT():
  listaPaises =[]
  f = open ('paises_prueba.txt','r')
  for x in f:
    temp = len(x)
    namePais = x[:temp - 1]
    listaPaises.append(namePais)
  return listaPaises

run() # invocaion de el meto para correr

